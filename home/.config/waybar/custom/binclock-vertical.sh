#!/bin/sh

# Get the time
_hour=$(date +%H)
_min=$(date +%M)
_sec=$(date +%S)

# Separate each number into single digit variables
_h1=$((10#$_hour/10))
_m1=$((10#$_min/10))
_s1=$((10#$_sec/10))
_h2=$((10#$_hour-($_h1*10)))
_m2=$((10#$_min-($_m1*10)))
_s2=$((10#$_sec-($_s1*10)))

# Input:
# $1=Number of characters to use for output minus one
# $2=Number to convert.
# Output:
# String of $1 +1 characters with # for 1 and - for 0
scannum(){
	a="$(echo 2^"$1" | bc)"
	b=$2
	c=""
	while [ "$a" -gt "0" ]
	do
		if [ "$b" -ge "$a" ]
		then
			c="$c#"
			b=$((b-a))
		else
			c="$c-"
		fi
		a=$((a/2))
	done
	echo $c
}

