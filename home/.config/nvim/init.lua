local vim = vim -- Make the warnings shut up
local keyset = vim.keymap.set
-- Plugins
local Plug = vim.fn['plug#']   -- import Plug from vim-plug
vim.call('plug#begin')         -- begin importation of plugins
	Plug('neovim/nvim-lspconfig')  -- LSPconfig
	Plug('neoclide/coc.nvim',      -- Conquer of Completion
		{ ['branch'] = 'release' })
	Plug('rust-lang/rust.vim')     -- Rust for Vim
	Plug('dense-analysis/ale') --[[Asynchronous Lint Engine
                                              (syntax checker)]]
	Plug('ellisonleao/gruvbox.nvim')   -- Gruvbox color scheme
	Plug('nvim-lualine/lualine.nvim')  -- Lualine (status line)
	Plug('preservim/nerdtree')		-- NERDRree - tree explorer
vim.call('plug#end')               -- end importation of plugins

-- -- Programming
-- Rust
vim.g.rustfmt_autosave = 1
vim.g.rustfmt_emit_files = 1
vim.g.rustfmt_fail_silently = 0

-- -- CoC

-- Extensions

vim.cmd([[
let g:coc_global_extensions = [
         \ 'coc-rust-analyzer',
         \ 'coc-sumneko-lua',
         \ 'coc-pyright',
         \ 'coc-markdownlint',
         \ 'coc-css',
         \ 'coc-clangd',
         \ 'coc-cmake',
         \ 'coc-html',
         \ 'coc-json',
         \ 'coc-git',
         \ 'coc-prettier',
		 \ 'coc-yaml',
		 \ 'coc-toml']
]])

-- Completion binds

local opts = { silent = true, expr = true, noremap = true, replace_keycodes = false }
keyset("i", "<c-space>", "coc#refresh()", opts) -- Ctrl + Space triggers completion
keyset("i", "<cr>", [[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]], opts)

local opts = { silent = true, nowait = true }
-- Apply codeAction to the selected region
keyset("x", "<leader>a", "<Plug>(coc-codeaction-selected)", opts)
keyset("n", "<leader>a", "<Plug>(coc-codeaction-selected)", opts)
-- Remap keys for apply code actions at the cursor position.
keyset("n", "<leader>ac", "<Plug>(coc-codeaction-cursor)", opts)
-- Remap keys for apply source code actions for current file.
keyset("n", "<leader>as", "<Plug>(coc-codeaction-source)", opts)
-- Apply the most preferred quickfix action on the current line.
keyset("n", "<leader>qf", "<Plug>(coc-fix-current)", opts)
-- Format selected code
keyset("x", "<leader>f", "<Plug>(coc-format-selected)", { silent = true })
keyset("n", "<leader>f", "<Plug>(coc-format-selected)", { silent = true })
-- Remap keys for apply refactor code actions.
keyset("n", "<leader>re", "<Plug>(coc-codeaction-refactor)", { silent = true })
keyset("x", "<leader>r", "<Plug>(coc-codeaction-refactor-selected)", { silent = true })
keyset("n", "<leader>r", "<Plug>(coc-codeaction-refactor-selected)", { silent = true })
vim.opt.updatetime = 300   -- Faster update time (default 4s which is awful)
vim.opt.signcolumn = "yes" -- Always show the signcolumn, otherwise it would shift the text each time

-- Misc
vim.api.nvim_set_option("clipboard", "unnamedplus")   -- use system clipboard if available
vim.opt.completeopt = { 'menu', 'menuone', 'noselect' } -- menu selection
vim.opt.mouse = 'a'                                   -- allow the mouse to be used

-- UI config
vim.opt.number = true -- show absolute number
vim.opt.relativenumber = true --[[add numbers to each line
                                              on the left side]]
vim.opt.cursorline = true --[[highlight cursor line underneath
                                              the cursor horizontally ]]
vim.opt.splitbelow = true -- open new vertical split bottom
vim.opt.splitright = true -- open new horizontal splits right
vim.opt.showmode = false --[[we are experienced, wo don't need
                                              the "-- INSERT --" mode hint]]

-- Searching
vim.opt.incsearch = true  -- search as characters are entered
vim.opt.hlsearch = false  -- do not highlight matches
vim.opt.ignorecase = true -- ignore case in searches by default
vim.opt.smartcase = true --[[but make it case sensitive if
                                              an uppercase is entered]]

-- Column
vim.opt.colorcolumn = "80" -- show a line at coulumn 80

-- Tab
vim.opt.tabstop = 4 -- number of visual spaces per TAB
vim.opt.softtabstop = 4 --[[number of spacesin
                                              tab when editing]]
vim.opt.shiftwidth = 4    -- insert 2 spaces on a tab
vim.opt.expandtab = false -- I hate spaces

-- Indentation
vim.opt.smartindent = true

-- Lualine
require('lualine').setup{
	options = {
		theme = 'gruvbox'
	},
	sections = {
		lualine_x = {'g:coc_status', 'encoding', 'fileformat', 'filetype'}
	}
}
require('lualine').setup()

-- NERDTree
vim.cmd([[
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif
]])

-- Colors
vim.o.background = "dark"
vim.cmd([[colorscheme gruvbox]])
