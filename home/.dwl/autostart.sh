#!/bin/sh

source /tmp/dbus-current
export DBUS_SESSION_BUS_ADDRESS
export DBUS_SESSION_BUS_PID

function fp() {
	flatpak run $@ &
}


function autorun() {
	swaybg -o 'DP-3' -i "/home/user/Pictures/Wallpapers/Experiments/left.png" -m fill &
	swaybg -o 'DP-1' -i "/home/user/Pictures/Wallpapers/Experiments/center.png" -m fill &
	swaybg -o 'HDMI-A-1' -i "/home/user/Pictures/Wallpapers/Experiments/bottom.png" -m fill &
	swaybg -o 'DP-2' -i "/home/user/Pictures/Wallpapers/Experiments/right.png" -m fill &
	waybar &
	gentoo-pipewire-launcher restart &
	/usr/libexec/xdg-desktop-portal-gtk -r &
	/usr/libexec/xdg-desktop-portal-wlr -r &
	/usr/libexec/flatpak-portal -r &
	sleep 5
	dbus-update-activation-environment --all
	gnome-keyring-daemon --start --components=secrets &
	mako &
	ckb-next -b &
	/usr/libexec/polkit-gnome-authentication-agent-1 &		
	fp org.keepassxc.KeePassXC
	fp net.cozic.joplin_desktop
	BeamMP-Launcher --no-launch &
	ollama serve &
	sleep 3 && brave --profile-directory=Default --app-id=ejhkdoiecgkmdpomoahkdihbcldkgjci &
	brave --profile-directory=Default --app-id=magkoliahgffibhgfkmoealggombgknl &
}

autorun >/dev/null 2>&1
