#
#	░▒▓███████▓▒░ ░▒▓██████▓▒░ ░▒▓███████▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓███████▓▒░ ░▒▓██████▓▒░
#	░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░      ░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░
#	░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░      ░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░
#	░▒▓███████▓▒░░▒▓████████▓▒░░▒▓██████▓▒░░▒▓████████▓▒░▒▓███████▓▒░░▒▓█▓▒░
#	░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░	 ░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░
#░▒▓██▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░	 ░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░
#░▒▓██▓▒░▒▓███████▓▒░░▒▓█▓▒░░▒▓█▓▒░▒▓███████▓▒░░▒▓█▓▒░░▒▓█▓▒░▒▓█▓▒░░▒▓█▓▒░░▒▓██████▓▒░
#
#
#

EDITOR=vi

if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
	PATH="$PATH:$HOME/.local/bin:$HOME/bin"
fi


if ! [[ "$PATH" =~ "/var/lib/flatpak/exports/bin:$HOME/.local/share/flatpak/exports/bin" ]]
then
	PATH="$PATH:/var/lib/flatpak/exports/bin:$HOME/.local/share/flatpak/exports/bin"
fi

if [ -e /tmp/dbus-current ]; then
	source /tmp/dbus-current
	export DBUS_SESSION_BUS_ADDRESS
	export DBUS_SESSION_BUS_PID
fi


if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

for f in ~/.bash_*
do
	if [ -O "$f" ]
	then
		rm -f $f
	fi
done

# TERMINAL COLORS (tty)
if [ "$TERM" = "linux" ]; then
  echo -e "
  \e]P0202020
  \e]P1bf3f34
  \e]P2547d22
  \e]P39d7722
  \e]P4386892
  \e]P56a2a81
  \e]P6389b79
  \e]P778746c
  \e]P8565656
  \e]P9ff6c5f
  \e]PA9aca60
  \e]PBd5ac50
  \e]PC97b9d7
  \e]PDb274c8
  \e]PE88ddc0
  \e]PFd7d0c2
  "
  # get rid of artifacts
  clear
fi

# BEMENU COLORS
BEMENU_BG='#15110e'
BEMENU_FG='#edd19a'
BEMENU_COL='#e3963e'
export BEMENU_OPTS="--fn 'MonaspiceKr Nerd Font Mono 12' 
	--ch 4 --cw 4 
	--bdr $BEMENU_COL 
	--tb $BEMENU_COL 
	--tf $BEMENU_BG 
	--fb $BEMENU_COL 
	--ff $BEMENU_BG 
	--nb $BEMENU_BG 
	--nf $BEMENU_FG 
	--hf $BEMENU_COL 
	--hb $BEMENU_BG 
	--af $BEMENU_FG 
	--ab $BEMENU_BG 
	--cf $BEMENU_BG 
	"

if [ -n "${TERMFM}" ]
then
	ls -lah --color
fi

# COLOR MANPAGES
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-R

if [ -z "${WAYLAND_DISPLAY}" ] &&
		[ "$(tty)" = "/dev/tty1" ] || [ "$(tty)" =	"/dev/ttyv0" ] ||
		[ "$(tty)" = "/dev/ttyC0" ]; then
	export XDG_SESSION_TYPE=wayland
	export QT_QPA_PLATFORM=wayland
	export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
	#export WLR_RENDERER=vulkan
	export VDPAU_DRIVER=radeonsi
	export WLR_NO_HARDWARE_CURSORS=1
	export XDG_CURRENT_DESKTOP=wlroots
	dbus-launch --exit-with-session > /tmp/dbus-current
	source /tmp/dbus-current
	export DBUS_SESSION_BUS_ADDRESS
	export DBUS_SESSION_BUS_PID
	uname -sr | dwl
	exit
fi


# Competion for doas
complete -F _root_command doas

# Replacements, shortcuts, etc...
alias ls="ls -ah --color"
alias x="exit"
alias e=$EDITOR

#Tmux
alias ta="TMUX='' tmux attach -t" # Attach (force it)
alias tc="echo 'Current tmux: $TMUX'" # Print current tmux
alias tl="tmux list-sessions" # List tmux sessions
alias tk="tmux kill-session -t" # Kill a tmux sessions

# Prompt (starship.rs)
eval "$(starship init bash)"

# Start tmux
if [ -z "${TMUX}" ]
then
	exec tmux
else
	export TERM=screen-256color
fi


export PATH=$PATH:/home/user/.spicetify
. "$HOME/.cargo/env"
