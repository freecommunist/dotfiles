#!/bin/sh
# Yes, I recycled this from the older dotfiles, so what ?
FILES_TO_COPY="
	/etc/security/limits.d/60-esync.conf
"
DIRS_TO_COPY="
	/etc/portage
"
for f in $FILES_TO_COPY
do
        mkdir -pv ./root/$(dirname $f)
        cp -uv $f ./root/$f
done

for f in $DIRS_TO_COPY
do
    mkdir -pv ./root/$f
    cp -urv $f/* ./root/$f/
done
