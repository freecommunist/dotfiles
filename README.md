# dotfiles
My dotfiles. Feel free to reuse them as you will, it's public for a reason :)

Tested to just work™️ on my machine (Gentoo with dwl)

All the wallpapers under Pictures/Wallpapers are made by me and are under the [Creative Commons Attribution-Sharealike 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

![Here is a screenshot of my setup](./screenshot.png)
